import { createApp } from 'vue'
import App from './App.vue'
import '../src/base.css'

createApp(App).mount('#app')
